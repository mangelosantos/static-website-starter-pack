'use strict';

const gulp = require('gulp');
const pump = require('pump');

const $ = require('gulp-load-plugins')({
  postRequireTransforms: {
    typescript: function(typescript) {
      return typescript.createProject('tsconfig.json');
    }
  }
});

// Compile, bundle, and minify SASS files.
gulp.task('sass', function(error) {
  pump([
    gulp.src(['app/scss/*.scss']),
    $.sassLint(),
    $.sassLint.format(),
    $.sass({
      outputStyle: 'compressed'
    }).on('error', $.sass.logError),
    $.concat('core.bundle.min.css'),
    gulp.dest('dist/css')
  ], error);
});

// Compile, bundle, and minify Typescript files.
gulp.task('ts', function(error) {
  pump([
    $.typescript.src(),
    $.typescript(),
    $.uglify(),
    $.concat('core.bundle.min.js'),
    gulp.dest('dist/js')
  ], error);
});

// Compile, bundle, and minify Handlebars template files.
gulp.task('template', function(error) {
  pump([
    gulp.src(['app/hbs/*.hbs']),
    $.handlebars({
      handlebars: require('handlebars')
    }),
    $.wrap('Handlebars.template(<%= contents %>)'),
    $.declare({
      namespace: 'MAS.templates',
      noRedeclare: true
    }),
    $.uglify(),
    $.concat('template.bundle.min.js'),
    gulp.dest('dist/js')
  ], error);
});

// Copy and bundle vendor minified CSS files from node_modules to dist folder.
gulp.task('copy-vendor-css', function(error) {
  pump([
    gulp.src([
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
    ]),
    $.sass({
      outputStyle: 'compressed'
    }).on('error', $.sass.logError),
    $.concat('vendor.bundle.min.css'),
    gulp.dest('dist/css')
  ], error);
});

// Copy and bundle vendor minified JS files from node_modules to dist folder.
gulp.task('copy-vendor-js', function(error) {
  pump([
    gulp.src([
      'node_modules/handlebars/dist/handlebars.runtime.min.js',
      'node_modules/jquery/dist/jquery.slim.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.min.js'
    ]),
    $.concat('vendor.bundle.min.js'),
    gulp.dest('dist/js')
  ], error);
});

// Copy static project dependencies to dist folder.
gulp.task('copy-dependencies', function() {
  gulp.src(['app/data/**/*', 'app/img/**/*', 'app/*.html'], { base: './app/' })
    .pipe(gulp.dest('dist'));
});

gulp.task('minify', ['sass', 'ts'], function() {
  return;
});

// Start the server, watch for file changes, and trigger a page reload as necessary.
gulp.task('serve', ['minify', 'template', 'copy-vendor-css', 'copy-vendor-js', 'copy-dependencies'], function() {
  const server = $.liveServer.static('dist');
  server.start();

  gulp.watch(['app/scss/*.scss'], ['sass']);
  gulp.watch(['app/ts/*.ts'], ['ts']);
  gulp.watch(['app/hbs/*.hbs'], ['template']);
  gulp.watch(['app/data/**/*', 'app/img/**/*', 'app/*.html'], ['copy-dependencies']);
  gulp.watch(['dist/css/*.css', 'dist/js/*.js', 'dist/data/*.json', 'dist/*.html'], function (file) {
    server.notify.apply(server, [file]);
  });
});

gulp.task('default', ['serve']);
